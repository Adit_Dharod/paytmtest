package paytm;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.testng.Assert;

public class MovieGet {

	public static void main(String[] args) throws ParseException {
		// TODO Auto-generated method stub
		
		RestAssured.baseURI= "https://apiproxy.paytm.com";
		String response = given().log().all()
		.when().get("/v2/movies/upcoming")
		.then().assertThat().log().all().statusCode(200).extract().response().asString();
		
		JsonPath js= new JsonPath(response);
		//js.getString(path)
		
		int count = js.getInt("upcomingMovieData.size()");
		for (int i=0;i<count;i++)
		{
			String moviename = js.get("upcomingMovieData["+i+"].provider_moviename");
			String moviePosterURL = js.get("upcomingMovieData["+i+"].moviePosterUrl");
			if(moviePosterURL.endsWith(".jpg"))
			{
				Assert.assertTrue(true);
			}
			else{
				//String moviename = js.get("upcomingMovieData["+i+"].provider_moviename");
				System.out.println(" The " + moviename + "is not having proper poster format");				
				Assert.assertFalse(true);
			}
			 int contentAvailable = js.getInt("upcomingMovieData["+i+"].isContentAvailable");
			 if (contentAvailable == 0)
			 {
				 //String moviename = js.get("upcomingMovieData["+i+"].provider_moviename");
				 System.out.println("Content available for "+moviename+" is 0");
			 }
			 
			 SimpleDateFormat sdfo = new SimpleDateFormat("yyyy-MM-dd");
			 String date = js.get("upcomingMovieData["+i+"].releaseDate");
			 Date moviesReleaseDate = sdfo.parse(date);
			 Date todayDate = new Date();
			 sdfo.format(todayDate);
			 
			 if(moviesReleaseDate.after(todayDate))
			 {
				 System.out.println();
			 }
			 else if(moviesReleaseDate.before(todayDate))
			 {
				 Assert.assertTrue(false);
				 System.out.println("Fail" + moviename+" is releasing before todays date");
			 }
			 else if(moviesReleaseDate.equals(todayDate))
			 {
				 System.out.println();
			 }
			 
			 /*System.out.println(moviesReleaseDate +"  "+todayDate  );
			 String today = getToday ("yyyy-MM-dd")
			 Date todayDate = new Date();
			 Date d2 = sdfo.parse(todayDate);
			 LocalDate current = java.time.LocalDate.now();
			 System.out.println(java.time.LocalDate.now());*/  

		} 

	}

}
